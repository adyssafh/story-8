from django.test import TestCase

# Create your tests here.
# Create your tests here
class lab8UnitTest(TestCase):
	def test_lab8_url_is_exist(self):
		response = Client().get(' ')
		self.assertEqual(response.status_code, 200)

	def test_lab8_urls_is_not_exist(self):
		response = self.client.get(' ')
		self.assertFalse(response.status_code==404)

	def test_lab8_using_landingpage_func(self):
		found = resolve(' ')
		self.assertEqual(found.func, index)

	def test_lab8_using_template(self):
		response = Client().get(' ')
		self.assertTemplateUsed(response, 'index.html')


